import os.path


def _parse(path, out):
    form = open(path, "r", encoding='utf8')
    string = form.readline()
    while (string):
        string = form.readline()
        if ("1.2. TITRE DU PROJET" in string):
            string = string.strip()
            out.write(string + " ")
            for i in range(3):
                string = form.readline()
            out.write(string + '\n')
            
        if ("2. RÉSUMÉ NON TECHNIQUE" in string):
            string = string.strip()
            out.write(string + " ")
            for i in range(5):
                string = form.readline()
            out.write(string + '\n')
            
        if ("4.2 Nombre de procédures expérimentales" in string):
            string = string.strip()
            out.write(string + " ")
            for i in range(2):
                string = form.readline()
            out.write(string + "\n")
            
        if ("4.2.1" in string):
            #is present multiple time
            string = string.strip()
            out.write(string + " ")
            for i in range(4):
                string = form.readline()
            out.write(string + "\n")
            
        if ("TOTAL Procédure" in string):
            #is present multiple time
            string = string.strip()
            out.write(string + " ")
            string = form.readline()
            out.write(string + '\n')
            
            
    form.close()
    
def parse():
    if (os.path.exists('./test/outTEST.txt')):
        os.remove("./test/outTEST.txt")
    out = open("./test/outTEST.txt", "w")
    _parse("./test/formulaire.txt", out)
    out.close()
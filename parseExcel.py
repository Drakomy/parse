import os.path
import pandas as pd

def _parse(path, out):
    form = open(path, "r", encoding='utf8')
    string = form.readline()
    out.write("titre,résumé non technique,nombre de procédures,numéro de procédure," \
              "titre procédure,nombre d'animaux\n")
        
    ctrl = True
    list1 = []
    while (string and ctrl):
        string = form.readline()
        if ("1.2. TITRE DU PROJET" in string):
            for i in range(3):
                string = form.readline()
            list1.extend([string.strip(), ","])
        
        if ("2. RÉSUMÉ NON TECHNIQUE" in string):
            for i in range(5):
                string = form.readline()
            list1.extend([string.strip(), ","])
            
        if ("4.2 Nombre de procédures expérimentales" in string):
            for i in range(2):
                string = form.readline()
            list1.extend([string.strip(), ","])
            str1 = "".join(str(x) for x in list1)
            ctrl = False
            
    
    count = 1;
    list2 = []
    while (string):
        string = form.readline()
        if ("4.2.1" in string):
            for i in range(4):
                string = form.readline()
            list2.extend([str(count), ",", string.strip(), ","])
            
        if ("TOTAL Procédure" in string):
            string = form.readline()
            list2.append(string.strip())
            str2 = "".join(str(x) for x in list2)
            out.write(str1 + str2 + "\n")
            count += 1
            list2 = []
    form.close()


def parse():
    if (os.path.exists('./test/out.txt')):
        os.remove("./test/out.txt")
    out = open("./test/out.txt", "w")
    _parse("./test/formulaire.txt", out)
    out.close()       
    
    
def parseExcel():
    parse()
    excel = "./test/out.txt"
    df = pd.read_csv(excel,sep=",", encoding = "latin_1", engine = "python")
    df.to_excel("./test/out.xlsx", "Sheet1", index=False)

if __name__ == "__main__":
    parseExcel()
    print("Form parsed to excel")
